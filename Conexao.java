package bd;

import java.sql.Statement;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Conexao {
	String home = System.getProperty("user.home");
	private Connection con = null;
	
	//construtor
	public Conexao() {
		String url = "jdbc:hsqldb:file:" + home + File.separator + "db";
		System.out.println("home:" + url);
		try { // tratamento de exce��o
			
			Class.forName("org.hsqldb.jdbcDriver");
			con = DriverManager.getConnection(url,"sa","");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void criarTabela() {
		String sql = " CREATE TABLE IF NOT EXISTS pessoa (nome varchar (100)) ";
		try {
		Statement stmt =con.createStatement();
		stmt.executeUpdate(sql); 
			
		    
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Tabela Criada");
	}
	
	public void inserirDado() {
		String sql = " INSERT INTO pessoa values ('julia') ";
		try {
		Statement stmt =con.createStatement();
		stmt.executeQuery(sql); 	    
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Registro inserido");
	}
	public void listarDados(){
		String sql = " SELECT *FROM pessoa";
		try {
		Statement stmt =con.createStatement();
		ResultSet rs = stmt.executeQuery(sql); //lista de resultados
		while (rs.next()) { // navega na cole��o
			System.out.println(rs.getString(1)); //1a coluna
		}
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("conte�do listado");
	}
}
